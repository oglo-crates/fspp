#![allow(dead_code)]

mod path;
pub use path::*;

#[cfg(feature = "filesystem")]
#[cfg(feature = "archive-module")]
/// Interacting with archive files
pub mod archive;

#[cfg(feature = "location")]
/// Get OS specific paths to places like home, config, cache, etc...
pub mod location;

#[cfg(feature = "filesystem")]
/// Interacting with files
pub mod file;

#[cfg(feature = "filesystem")]
/// Interacting with directories
pub mod directory;

#[cfg(feature = "filesystem")]
/// Moving, copying, deleting, etc... (works with both files and directories)
pub mod fs_action;

#[cfg(feature = "safe_filename_string")]
#[inline(always)]
/// Convert a string to something safe to use in a filename
pub fn filename_safe_string<S: AsRef<str>>(string: S) -> String {
    filenamify::filenamify(string)
}

#[test]
fn test() {
    if std::env::consts::OS == "windows" {
        assert!(Path::SPLIT_CHAR == '\\');
        assert!(Path::NOT_SPLIT_CHAR == '/');
    }

    else {
        assert!(Path::SPLIT_CHAR == '/');
        assert!(Path::NOT_SPLIT_CHAR == '\\');
    }
}
