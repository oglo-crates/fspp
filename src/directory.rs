use std::io;
use walkdir::WalkDir;
use super::Path;

/// List the items in a directory (not recursive)
pub fn list_items(path: &Path) -> Result<Vec<Path>, io::Error> {
    let mut items: Vec<Path> = Vec::new();

    for i in match std::fs::read_dir(path.to_string()) {
        Ok(o) => o,
        Err(e) => return Err(e),
    } {
        items.push(match i {
            Ok(o) => Path::new(o.path().display().to_string().as_str()),
            Err(e) => return Err(e),
        });
    }

    return Ok(items);
}

/// List the items in a directory (recursive)
pub fn list_items_recursive(path: &Path) -> Result<Vec<Path>, io::Error> {
    let mut items: Vec<Path> = Vec::new();

    for i in WalkDir::new(path.to_string()) {
        items.push(Path::new(&match i {
            Ok(o) => o,
            Err(_) => return Err(io::Error::new(io::ErrorKind::Other, "Failed in WalkDir somewhere!")),
        }.path().display().to_string()));
    }

    return Ok(items);
}

/// Create a new directory
pub fn create(path: &Path) -> Result<(), io::Error> {
    return match std::fs::create_dir_all(path.to_string()) {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    };
}
