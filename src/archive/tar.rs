use std::io;
use std::fs;
use tar::Builder;
use super::super::{Path, PathType, directory};

/// Create a tar archive
///
/// # Examples:
/// ```rust,ignore
/// use fspp::*;
///
/// let archive_path = Path::new("hello.tar");
/// let host_path = Path::new("folder1/folder2/folder3"); // Directory containing all the files/folders to add to the archive.
///
/// archive::tar::create(&archive_path, &host_path).unwrap();
/// ```
pub fn create(archive_path: &Path, paths_host: &Path) -> Result<(), io::Error> {
    let file = fs::File::create(archive_path.to_string())?;
    let mut builder = Builder::new(file);

    for i in directory::list_items(paths_host)? {
        if i.exists() == false {
            return Err(io::Error::new(io::ErrorKind::NotFound, "Tried adding a path that does not exist to tar archive!"));
        }

        let inside = i.to_string().replace(&format!("{}{sc}", paths_host.to_string(), sc = Path::SPLIT_CHAR), "");

        if i.path_type() == PathType::Directory {
            builder.append_dir_all(inside, i.to_string())?;
        }

        else {
            builder.append_path_with_name(i.to_string(), inside)?;
        }
    }

    return Ok(());
}
