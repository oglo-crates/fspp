use std::io;
use std::fs;
use std::path::PathBuf;
use super::super::Path;

/// Extract a zip archive
/// Arguments: (zip file path, destination path, strip top level)
///
/// # Examples:
/// ```rust,ignore
/// use fspp::*;
///
/// let target_path = Path::new("folder1/folder2/folder3");
/// let zip_path = target_path.add_str("stuff.zip");
///
/// // If the "strip top level" argument is set to false:
/// // The archive::zip::extract() function keeps everything as is
/// // If the "strip top level" argument is set to true:
/// // The archive::zip::extract() function strips away the entry folder
/// let zip_host_path = target_path.add_str("stuff_unzipped");
///
/// archive::zip::extract(&zip_path, &zip_host_path, true).unwrap();
/// archive::zip::extract(&zip_path, &target_path, false).unwrap();
/// ```
pub fn extract(archive_path: &Path, target_path: &Path, strip_toplevel: bool) -> Result<(), io::Error> {
    let archive = fs::File::open(archive_path.to_string())?;
    let target = PathBuf::from(target_path.to_string());

    match zip_extract::extract(archive, &target, strip_toplevel) {
        Ok(_) => (),
        Err(_) => return Err(io::Error::new(io::ErrorKind::Other, "Failed to unzip archive!")),
    };

    return Ok(());
}
