use std::io;
use std::fs;
use super::{Path, PathType};

/// Move a file or directory from point A, to point B!
pub fn mv(path_from: &Path, path_to: &Path) -> Result<(), io::Error> {
    copy(path_from, path_to)?;
    delete(path_from)?;

    return Ok(());
}

/// Copy a file.
pub fn copy(path_from: &Path, path_to: &Path) -> Result<(), io::Error> {
    let path_to_patched: Path;

    if path_to.path_type() == PathType::Directory && path_to.exists() {
        path_to_patched = Path::new(&format!("{}/{}", path_to.to_string(), path_from.basename()));
    }

    else {
        path_to_patched = path_to.clone();
    }
    
    if path_from.path_type() == PathType::File {
        match fs::copy(path_from.to_string(), path_to_patched.to_string()) {
            Ok(_) => (),
            Err(e) => return Err(e),
        };
    }

    else if path_from.path_type() == PathType::Directory {
        let mut options = fs_extra::dir::CopyOptions::new();

        options.copy_inside = true;

        match fs_extra::dir::copy(path_from.to_string(), path_to_patched.to_string(), &options) {
            Ok(_) => (),
            Err(_) => return Err(io::Error::new(io::ErrorKind::Other, "Failed to copy directory!")),
        };
    }

    else {
        return Err(io::Error::new(io::ErrorKind::NotFound, "Invalid path!"));
    }

    return Ok(());
}

/// Delete a file. (Be careful!)
pub fn delete(path: &Path) -> Result<(), io::Error> {
    if path.path_type() == PathType::File {
        match fs::remove_file(path.to_string()) {
            Ok(_) => (),
            Err(e) => return Err(e),
        };
    }

    else if path.path_type() == PathType::Directory {
        match fs::remove_dir_all(path.to_string()) {
            Ok(_) => (),
            Err(e) => return Err(e),
        };
    }

    else {
        return Err(io::Error::new(io::ErrorKind::NotFound, "Invalid path!"));
    }

    return Ok(());
}
