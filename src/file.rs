use std::fs::File;
use std::io::{self, Read, Write};
use super::Path;

/// Read the contents of a file
pub fn read(path: &Path) -> Result<String, io::Error> {
    let mut file = match File::open(path.to_string()) {
        Ok(o) => o,
        Err(e) => return Err(e),
    };

    let mut contents = String::new();

    match file.read_to_string(&mut contents) {
        Ok(_) => (),
        Err(e) => return Err(e),
    };

    return Ok(contents);
}

/// Write contents to a file
pub fn write(contents: &str, path: &Path) -> Result<(), io::Error> {
    let mut file = match File::create(path.to_string()) {
        Ok(o) => o,
        Err(e) => return Err(e),
    };

    match file.write_all(contents.as_bytes()) {
        Ok(_) => (),
        Err(e) => return Err(e),
    };

    return Ok(());
}
