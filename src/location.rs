use super::Path;

macro_rules! location_function {
    ($location: ident, $dir_func: ident, $win: literal, $mac: literal, $lin: literal, $red: literal) => {
        #[doc = concat!("| Windows: ", $win)]
        #[doc = concat!("| MacOS: ", $mac)]
        #[doc = concat!("| Linux: ", $lin)]
        #[doc = concat!("| Redox: ", $red, " |")]
        pub fn $location() -> Option<Path> {
            return match dirs::$dir_func() {
                Some(s) => Some(Path::new(&s.display().to_string())),
                None => None,
            };
        }
    };
}

location_function!(home, home_dir, "YES", "YES", "YES", "YES");
location_function!(cache, cache_dir, "YES", "YES", "YES", "YES");
location_function!(config, config_dir, "YES", "YES", "YES", "YES");
location_function!(config_local, config_local_dir, "YES", "YES", "YES", "YES");
location_function!(data, data_dir, "YES", "YES", "YES", "YES");
location_function!(data_local, data_local_dir, "YES", "YES", "YES", "YES");
location_function!(executable, executable_dir, "NO", "NO", "YES", "YES");
location_function!(preference, preference_dir, "YES", "YES", "YES", "YES");
location_function!(runtime, runtime_dir, "NO", "NO", "YES", "YES");
location_function!(state, state_dir, "NO", "NO", "YES", "YES");
location_function!(audio, audio_dir, "YES", "YES", "YES", "YES");
location_function!(desktop, desktop_dir, "YES", "YES", "YES", "YES");
location_function!(document, document_dir, "YES", "YES", "YES", "YES");
location_function!(download, download_dir, "YES", "YES", "YES", "YES");
location_function!(font, font_dir, "NO", "YES", "YES", "YES");
location_function!(picture, picture_dir, "YES", "YES", "YES", "YES");
location_function!(public, public_dir, "YES", "YES", "YES", "YES");
location_function!(template, template_dir, "YES", "NO", "YES", "YES");
location_function!(video, video_dir, "YES", "YES", "YES", "YES");
