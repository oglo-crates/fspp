# FSPP
FSPP stands for Filesystem++, and it is exactly what the name implies.
It is a better, more high-level library for filesystem tasks.
It also converts forward/backward slashes depending on the OS.

The crate contains functions for reading/writing files, unzipping zip archives, creating directories, etc...
It also makes the functions in fs_action work with both files and folders! Whereas the alternatives in STD are less intuitive,
and don't work with both files and folders, but rather one or the other. FSPP also has a function for moving files and folders,
whereas STD does not have that for some reason!

FSPP also can be super tiny! Everything is enabled/disabled via features,
so you could have FSPP only give you the `Path` struct and `PathType` enum! On the other hand,
you can also enable all the features for things like the `directory`, `file`, `fs_action` modules,
as well as `archive`, `archive::zip`, and `archive::tar`! By default FSPP only enables the `filesystem` feature,
which adds the modules: `file`, `directory`, `fs_action`.

If you want to use all the features, you can just add FSPP with the `full` feature!

Overall, FSPP aims to be a better, more intuitive, and easy to use version of the STD functions and add more functionality on top of that!



# Functionality:
```

 1. Creating files and folders.
 2. Deleting, copying, moving, etc...
 3. Interacting with zip and tar archives.

```



# Examples:
```rust
// Example: Reading, and writing a file. (And seeing what kind of path we have.)

use fspp::*;

fn main() {
  let path = Path::new("my_file.txt"); // FSPP path struct.

  if path.exists() { // Another way to see if a path exists is by seeing if the .path_type() method returns PathType::Invalid
    if path.path_type() == PathType::Directory {
      println!("Say whaaaat? The file is actually a directory?");
    }

    else if path.path_type() == PathType::File {
      let contents: String = file::read(&path).unwrap();

      println!("{}", contents);

      file::write("Hello, world!", &path).unwrap();
    }
  }

  else {
    println!("File not found!");
  }
}
```
```rust
// Example: Listing a directory.

use fspp::*;

fn main() {
  let path = Path::new("my_folder");

  // Listing items includes files, and directories.
  let items = directory::list_items(&path).unwrap();
  let items_recursive = directory::list_items_recursive(&path).unwrap();
}
```
```rust
// Example: Creating, deleting, moving, copying...

use fspp::*;

fn main() {
  let file_path = Path::new("my_file.txt");
  let folder_path = Path::new("my_folder");

  // If the file exists, it will be overwritten! (I should add a file::touch() function.)
  file::write("", &file_path).unwrap();

  directory::create(&folder_path).unwrap();

  let new_file_path = Path::new("my_folder/my_file.txt");

  fs_action::mv(&file_path, &new_file_path).unwrap(); // fs_action functions work on both files, and folders.

  fs_action::copy(&new_file_path, &file_path).unwrap();

  fs_action::delete(&folder_path).unwrap();
}
```
